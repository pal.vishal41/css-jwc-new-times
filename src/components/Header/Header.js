import React from 'react';
import styled from 'styled-components/macro';
import { Menu, Search, User } from 'react-feather';

import { QUERIES } from '../../constants';

import MaxWidthWrapper from '../MaxWidthWrapper';
import Logo from '../Logo';
import Button from '../Button';

const Header = () => {
  return (
    <HeaderWrapper>
      <SuperHeader>
        <Row>
          <ActionGroup>
            <button>
              <Search size={24} />
            </button>
            <button>
              <Menu size={24} />
            </button>
          </ActionGroup>
          <ActionGroup mobile>
            <button>
              <User size={24} />
            </button>
          </ActionGroup>
          <DesktopActionGroup>
            <Button>Subscribe</Button>
            <SubscribedLink href="/">Already a subscriber?</SubscribedLink>
          </DesktopActionGroup>
        </Row>
      </SuperHeader>
      <MainHeader>
        <Logo />
      </MainHeader>
    </HeaderWrapper>
  );
};

const HeaderWrapper = styled.header`
  position: relative;
`;

const SuperHeader = styled.div`
  padding: 16px 0;
  background: var(--color-gray-900);
  color: white;

  @media ${QUERIES.laptopAndUp} {
    background: transparent;
    color: black;
  }
`;

const Row = styled(MaxWidthWrapper)`
  display: flex;
  justify-content: space-between;
`;

const ActionGroup = styled.div`
  display: flex;
  gap: 24px;

  /*
    FIX: Remove the inline spacing that comes with
    react-feather icons.
  */
  svg {
    display: block;
  }

  @media ${QUERIES.laptopAndUp} {
    display: ${p => p.mobile ? 'none' : 'flex'};
  }
`;

const DesktopActionGroup = styled.div`
  display: none;

  @media ${QUERIES.laptopAndUp} {
    display: flex;
    flex-direction: column;
    gap: 4px;
  }
`;

const SubscribedLink = styled.a`
  font-family: var(--font-family-serif);
  font-size: 0.875rem;
  text-decoration: underline;
  color: var(--color-gray-900);
  text-align: center;
  font-style: italic;
`;

const MainHeader = styled(MaxWidthWrapper)`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 32px;
  margin-bottom: 48px;

  @media ${QUERIES.laptopAndUp} {
    margin: 0;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    margin-inline: auto;
  }
`;

export default Header;
